//
//  ShowTripConfigurator.swift
//  tripStarWars
//
//  Created by rboboti on 14/11/2018.
//  Copyright © 2018 rboboti. All rights reserved.
//

import UIKit


extension ShowTripViewController : ShowTripPresenterOutput {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        self.router.routeToShowTripDetail(segue: segue)
    }
    
}

extension ShowTripInteractor : ShowTripOutput {}
extension ShowTripPresenter  : ShowTripInteractorOutput{}

class ShowTripConfigurator {
    
    static let instance = ShowTripConfigurator()
    
    private init(){}
    
    func configure(viewController: ShowTripViewController)
    {
        
        let presenter    = ShowTripPresenter()
        presenter.output = viewController
        
        let interactor  = ShowTripInteractor()
        interactor.output = presenter
        
        let router = ShowTripRouter()
        router.viewController = viewController
        router.dataStore      = interactor 
        
        viewController.output = interactor
        viewController.router = router   
    }
    
    
}
