//
//  ShowTripPresenter.swift
//  tripStarWars
//
//  Created by rboboti on 14/11/2018.
//  Copyright (c) 2018 rboboti. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

protocol ShowTripPresenterInput {
    func show(response:ShowTrip.GetTrip.Response)
}

protocol ShowTripPresenterOutput : class {
    func showTripDetail(viewModel:ShowTrip.GetTrip.ViewModel)
}

class ShowTripPresenter: ShowTripPresenterInput
{
    var output : ShowTripPresenterOutput!
    func show(response: ShowTrip.GetTrip.Response) {
        
        let trip = response.trip
        let displayTrip = ShowTrip.GetTrip.ViewModel.DisplayTrip(
            pilotName:        trip.pilot.name,
            departureStation: trip.pick_up.name,
            arrivalStation:   trip.drop_off.name,
            startTime:        trip.pick_up.date.time,
            endTime:          trip.drop_off.date.time,
            distanceTrip:     "\(trip.distance.value.formattedWithSeparator) " + trip.distance.unit,
            durationTrip: "\(trip.duration.duration)",
            rating: trip.pilot.rating
        )
        
        let viewModel = ShowTrip.GetTrip.ViewModel(displayTrip: displayTrip, trip: trip)
        output.showTripDetail(viewModel: viewModel)   
    }
 
}
