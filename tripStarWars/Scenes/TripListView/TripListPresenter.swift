//
//  TripListPresenter.swift
//  tripStarWars
//
//  Created by rboboti on 10/11/2018.
//  Copyright (c) 2018 rboboti. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit



protocol TripListPresenterInput{
     func presentTrips(_ response:TripsResponse) 
 }
 
 protocol TripListPresenterOutput : class {
     func displayTrips(_ response:TripsViewModel)
  
 }
 
 
class TripListPresenter : TripListPresenterInput
{
    
    weak var output: TripListPresenterOutput! 
    
    func presentTrips(_ response: TripsResponse) {
        let viewModel = TripsViewModel(trips: response.trips)
        output.displayTrips(viewModel)   
    }
    
}
