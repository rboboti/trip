//
//  TripListConfiguarator.swift
//  tripStarWars
//
//  Created by rboboti on 12/11/2018.
//  Copyright © 2018 rboboti. All rights reserved.
//

import UIKit


extension TripListViewController : TripListPresenterOutput {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
      self.router.routeToShowTripDetail(segue: segue)  
    }
    
} 

extension TripListInteractor : TripListBusinessLogic {}
extension TripListPresenter  : TripsListInteractorOutput{}

class TripListConfigurator {
    
    static let instance = TripListConfigurator()
    
    private init() {
        
    }
    
    func configure(viewController: TripListViewController)
    {
  
        let presenter    = TripListPresenter()
        presenter.output = viewController
        
        let interactor = TripListInteractor()
        interactor.output = presenter
        
        let router = TripListRouter()
        router.viewController = viewController
        router.dataStore      = interactor
        
        viewController.output = interactor
        viewController.router = router
    }
}
