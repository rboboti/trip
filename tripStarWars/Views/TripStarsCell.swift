//
//  TripStarsCell.swift
//  tripStarWars
//
//  Created by rboboti on 12/11/2018.
//  Copyright © 2018 rboboti. All rights reserved.
//

import UIKit
import Cosmos
import Kingfisher

class TripStarsCell: UITableViewCell {

    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var departureLabel: UILabel!
    @IBOutlet weak var endTripLabel: UILabel!
    @IBOutlet weak var cosmosView: CosmosView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureCell(trip : TripViewModel)
    {
        nameLabel.text      = trip.trip.pilot.name
        departureLabel.text = trip.trip.pick_up.name
        endTripLabel.text   = trip.trip.drop_off.name
        cosmosView.rating   = trip.trip.pilot.rating
        
        let urlImage = basePlanetURL + trip.trip.pilot.avatar
        DispatchQueue.main.async(execute: {
            self.icon.kf.setImage(with: URL(string: urlImage))
        })
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    override func prepareForReuse() {
        cosmosView.prepareForReuse() 
    }
    
}
