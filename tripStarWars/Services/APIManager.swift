//
//  APIManager.swift
//  tripStarWars
//
//  Created by rboboti on 10/11/2018.
//  Copyright © 2018 rboboti. All rights reserved.
//

import Foundation
import Alamofire
import RxSwift

let baseURL = "https://starwars.chauffeur-prive.com/trips"
let basePlanetURL = "https://starwars.chauffeur-prive.com"   

enum NetworkError: Error {
    case invalidResponse, parsingFailure
    var desc: String {
        switch self {
        case .invalidResponse:
            return "invalid response"
        case .parsingFailure:
            return "parsing failure"
        }
    }
}

class APIManager: NSObject {
    
    static let sharedInstance = APIManager()
    
    func getTrips(url:String, completion:@escaping( ([Trip]?, Error?) -> () ))
    {
         let url = URL(string: url)
        
        
        Alamofire.request(url!).responseJSON {
             response in
            
            if response.response?.statusCode == 200 {
                switch response.result {
                case .success:
                    guard
                        let data = response.data 
                    else  { return }
                    
                    do{
                
                        let decoder = JSONDecoder()
                        let trips   = try? decoder.decode([Trip].self, from: data) as? [Trip]
                        completion(trips!, nil)
                    }catch{
                        print("Erreur")
                        completion(nil, response.error) 
                    }
                case .failure:
                    completion(nil, response.error)
                    break
                }
            }
        }
    }
    

    func getTrips(urlString:String ) -> Observable<[Trip]>{
        return Observable.create({
            observer -> Disposable in
            
            let url = URL(string: urlString)
            Alamofire.request(url!).responseJSON(completionHandler: {
                response in
                
                if response.response?.statusCode == 200 {
                    switch response.result {
                    case .success:
                        guard
                            let data = response.data
                        else {
                            observer.onError(NetworkError.invalidResponse)
                            return
                        }
                        
                        let decoder = JSONDecoder()
                        do{
                            let trips   = try? decoder.decode([Trip].self, from: data)
                            let sortedTrips = trips?.sorted(by: {
                                $0.pilot.rating < $1.pilot.rating 
                            })
                            
                            DispatchQueue.main.async {
                                observer.onNext(sortedTrips!)
                            }
                            
                        }catch{
                            observer.onError(NetworkError.parsingFailure)
                            return
                        }
                    case .failure(let error):
                        observer.onError(error)
                    }
                }
                
            })
            return Disposables.create()
        })
    }
    
}
