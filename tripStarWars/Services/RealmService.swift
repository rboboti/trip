//
//  RealmService.swift
//  tripStarWars
//
//  Created by rboboti on 16/01/2019.
//  Copyright © 2019 rboboti. All rights reserved.
//

import RealmSwift

class RealmService:NSObject {
    
    
    static let sharedInstance = RealmService()
    let realm = try! Realm()
    
    
    func add(object:Object)
    {
        do{
            try realm.write({
                realm.add(object)
            })
        }catch{
            print(" issue to save ")
        }
    }
    
    func add(objects:[Object])
    {
        do{
            try realm.write({
                realm.add(objects)
            })
        }
        catch
        {
            print(" issue to save ")
        }
    }
    
    func get(objectType:Object.Type) -> Results<Object>?{
        let objects = realm.objects(objectType)
        if objects.isEmpty {
            return nil
        }
        return objects
    }
    
    func remove(object:Object)
    {
        realm.beginWrite()
        realm.delete(object)
        do{
           try realm.commitWrite()
        }catch{
            print(" issue to save ")
        }
    }
    
    func removeAll(objectType:Object.Type){
        let objects = realm.objects(objectType)
        
        realm.beginWrite()
        realm.delete(objects)
        do{
            try realm.commitWrite()
        }catch{
            print(" issue to save ")
        }
    } 
    
}
